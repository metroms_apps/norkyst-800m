########################################################################
# Python-modules:
########################################################################
import numpy as np
import os
from datetime import datetime, timedelta
import netCDF4
########################################################################
# METROMS-modules:
########################################################################
import Constants
from GlobalParams import *
from Params import *
from ModelRun import *
########################################################################
########################################################################
# Set cpus for ROMS:
xcpu=18
ycpu=16
# Choose a predefined ROMS-application:
app='norkyst-800m'

# Copy atm. forcing:
os.system('cp -av /prod/forecast/run/NorKyst-800m/ocean_force_NorKyst800.nc '+GlobalParams.RUNDIR+"/"+app+"/stripe/ocean_force.nc")
ref_time    = netCDF4.Dataset(GlobalParams.RUNDIR+"/"+app+"/stripe/ocean_force.nc").variables['forecast_reference_time']
# Find start- and enddate:
start_date  = netCDF4.num2date(ref_time[:], ref_time.units)
end_date    = start_date + timedelta(hours=48)
print start_date, end_date

n800params=Params(app,xcpu,ycpu,start_date,end_date,nrrec=1,restart=True)

modelrun=ModelRun(n800params)

# Delete old ini-files:
os.system('find '+n800params.RUNPATH+'/ocean_ini.nc_* -mtime +8 -exec rm {} \;')
os.system('find '+n800params.RUNPATH+'/run.log_* -mtime +8 -exec rm {} \;')
#
modelrun.preprocess()
modelrun.run_roms(Constants.MPI,Constants.NODEBUG,Constants.VILJE)
#modelrun.run_roms(Constants.DRY,Constants.NODEBUG,Constants.VILJE)
#os.system('module load nco; ncks -A -v forecast_reference_time '+n800params.ATMFILE+' '+n800params.RUNPATH+'/stripe/ocean_his.nc')
os.system('python ${METROMS_APPDIR}/norkyst-800m/add_forecast_ref_time.py '+n800params.RUNPATH+'/stripe/ocean_his.nc '+n800params.ATMFILE)
os.system('python ${METROMS_APPDIR}/norkyst-800m/add_forecast_ref_time.py '+n800params.RUNPATH+'/stripe/ocean_avg.nc '+n800params.ATMFILE)
os.system('chmod a+r '+n800params.RUNPATH+'/stripe/*')
#modelrun.postprocess()

