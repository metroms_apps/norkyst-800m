#!/bin/sh
#
#SBATCH --job-name=norkyst800
#SBATCH --time=03:00:00
#SBATCH -A met
#SBATCH --ntasks-per-node=16
#SBATCH --nodes=32
#SBATCH -o /home/metno_op/run/norkyst-800m_2017/tmp/NorKyst-800m00_prep_o.log
#SBATCH -e /home/metno_op/run/norkyst-800m_2017/tmp/NorKyst-800m00_prep_e.log
#SBATCH --exclusive
set -x
nodes=32
. $HOME/status/Status_Funcs.sh
HOST=${HOST-`uname -n`}
jobname='NorKyst-800m00 - 2017 - ROMS 3.7 : 800m'
trap 'Runstatus "FAILED ..." "$jobname" $nodes; exit 99' 0
Runstatus "start ...." "$jobname" $nodes
#
source $HOME/sea/ROMS/metroms/apps/myenv.bash alvin
# Load modules needed
source ${METROMS_BASEDIR}/apps/modules.sh
datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>${METROMS_TMPDIR}/norkyst-800m_2017/run.log_${datstamp} 2>&1
ln -sf ${METROMS_TMPDIR}/norkyst-800m_2017/run.log_${datstamp} ${METROMS_TMPDIR}/norkyst-800m_2017/run.log
module list
#
cd ${METROMS_APPDIR}/norkyst-800m_2017
# Copy atm. forc. and change from spec to rel hum:
#cp -av /home/metno_op/run/NorKyst-800m/ocean_force_NorKyst800.nc ${METROMS_TMPDIR}/norkyst-800m_2017/ocean_force.nc
#python ${METROMS_BASEDIR}/tools/spec_to_rel_hum.py ${METROMS_TMPDIR}/norkyst-800m_2017/ocean_force.nc
ln -sf ${METROMS_TMPDIR}/norkyst-800m_2017/ocean_force_NorKyst800.nc ${METROMS_TMPDIR}/norkyst-800m_2017/ocean_force.nc
#export MPI_BUFS_PER_PROC=256
#export MPI_BUFS_PER_HOST=1024
ls -lh ${METROMS_TMPDIR}/norkyst-800m_2017/
python forecast_norkyst800m.py
#
Runstatus "end ......" "$jobname" $nodes
#
set +x
trap 0
exit
