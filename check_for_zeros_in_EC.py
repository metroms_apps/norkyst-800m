import sys
import netCDF4
import os

nc   = netCDF4.Dataset(sys.argv[1],'r+')
pair = nc.variables['Pair']
for t in range(pair.shape[0]):
   if pair[t,-1,0] == 0.0:
       print 'zero in '+str(t)
       for var in nc.variables:
           if len(nc.variables[var].shape) == 3:
               print 'fix '+var
               nc.variables[var][t,:,:] = nc.variables[var][t-1,:,:]
               nc.sync()
nc.close()
       
