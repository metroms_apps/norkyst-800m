#PBS -N norkyst800
#PBS -A mipa01hi
#PBS -l walltime=240:00:00
#PBS -l select=18:ncpus=32:mpiprocs=16:ompthreads=16:mem=29gb
#PBS -j oe
#PBS -o /prod/forecast/run/norkyst-800m_2017/o.log
###PBS -V
set -x
nodes=27
#. $HOME/atmos/um/Scripts/Model_Funcs.sh
. $HOME/status/Status_Funcs.sh
HOST=${HOST-`uname -n`}
jobname='NorKyst-800m00 - 2017 - ROMS 3.7 : 800m'
trap 'Runstatus "FAILED ..." "$jobname" $nodes; exit 99' 0
Runstatus "start ...." "$jobname" $nodes
#
source /prod/forecast/sea/ROMS/metroms/apps/forecast_myenv.bash
# Load modules needed
source /etc/profile.d/modules.sh
source ${METROMS_BASEDIR}/apps/modules.sh
datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>${METROMS_TMPDIR}/norkyst-800m_2017/run.log_${datstamp} 2>&1
ln -sf ${METROMS_TMPDIR}/norkyst-800m_2017/run.log_${datstamp} ${METROMS_TMPDIR}/norkyst-800m_2017/run.log
module list
#
cd ${METROMS_APPDIR}/norkyst-800m_2017
export MPI_BUFS_PER_PROC=256
export MPI_BUFS_PER_HOST=1024
python forecast_norkyst800m.py
#
Runstatus "end ......" "$jobname" $nodes
#
set +x
trap 0
exit
