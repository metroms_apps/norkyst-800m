import netCDF4
import sys
ifile = sys.argv[1]
ref_file = sys.argv[2]
print ifile, ref_file
nci = netCDF4.Dataset(ifile,'r+')
ncr = netCDF4.Dataset(ref_file)
# Copy variables
varin = ncr.variables['forecast_reference_time']
outVar = nci.createVariable('forecast_reference_time', 'd')
# Copy variable attributes
outVar.setncatts({k: varin.getncattr(k) for k in varin.ncattrs()})
outVar[:] = varin[:]
nci.close()
